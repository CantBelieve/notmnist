from __future__ import print_function
import matplotlib.pyplot as plt
import numpy as np
import os
import sys
import tarfile
import itertools
from IPython.display import display, Image
from scipy import ndimage
from sklearn.linear_model import LogisticRegression
from six.moves.urllib.request import urlretrieve
from six.moves import cPickle as pickle
import math
train_folders = ['.\\notMNIST_large\\A',
                 '.\\notMNIST_large\\B',
                 '.\\notMNIST_large\\C',
                 '.\\notMNIST_large\\D',
                 '.\\notMNIST_large\\E',
                 '.\\notMNIST_large\\F',
                 '.\\notMNIST_large\\G',
                 '.\\notMNIST_large\\H',
                 '.\\notMNIST_large\\I', 
                 '.\\notMNIST_large\\J']
test_folders = ['.\\notMNIST_small\\A',
                '.\\notMNIST_small\\B',
                '.\\notMNIST_small\\C',
                '.\\notMNIST_small\\D',
                '.\\notMNIST_small\\E',
                '.\\notMNIST_small\\F',
                '.\\notMNIST_small\\G',
                '.\\notMNIST_small\\H',
                '.\\notMNIST_small\\I',
                '.\\notMNIST_small\\J']
print("All Modules Has Been Imported...")

iteration_value = 1200
image_size = 28
pixel_depth = 255.0
train_sets_number = 17000
testing_set_value = 100

straight_line = "______________________________________________"

class Neural_Network:
  
  def __init__(self, directory = None):
    self.input_layer_size = (256, 1)
    self.output_layer_size = (10, 1)
    if (directory == None):
      self.w_matrix = np.random.uniform(-1 / math.sqrt(256), 1 / math.sqrt(256), (10, 784))
      print("Neural Network Generated New W-Matrix.")
    else:
      try:
        with open(directory, 'rb') as op:
          self.w_matrix = pickle.load(op)
      except Exception as Err:
        print("Can not find '.pickle' file in directory", directory, "or Something went wrong with '.pickling'.")
        print("Exiting...\n" + straight_line, "\n\n")
        raise SystemExit
    print(straight_line)
        
  def Cross_Entropy(self, softmax_matrix, answer):
    return (-1) * math.log(softmax_matrix[answer])
        
  def Classificate(self, image_array):
    try:
      if (image_array.shape != (784, 1)):
        print("Wrong size of input matrix, returning 'NONE'.")
        return None
    except AttributeError as Err:
      print("Invalid type of 'image_array', returning 'NONE'.")
      return None
    c_matrix = np.dot(self.w_matrix, image_array)
    return c_matrix
  
  def Softmax(self, matrix):
    return np.exp(matrix) / np.sum(np.exp(matrix), axis = 0)
  
  def Pickle_W(self, save_name):
    save_name = "W_matrix.pickle"
    try:
      with open(save_name, 'wb') as op_save:
        print("'.pickling' W-Matrix...")
        pickle.dump(self.w_matrix, op_save, pickle.HIGHEST_PROTOCOL)
    except Exception as Err:
      print("Unable to .pickle W-Matrix!")
  
  def Training(self, x_matrix_base):
    for it in range(0, 30):
      x_matrix = x_matrix_base[it][0]
      cur_ans = x_matrix_base[it][1]
      c_matrix = self.Classificate(x_matrix.reshape(784, 1))
      smax_matrix = self.Softmax(c_matrix)
      after_entropy_matrix =self.Cross_Entropy(smax_matrix, cur_ans)


def load_letter(folder, min_num_images):
  """Load the data for a single letter label."""
  image_files = os.listdir(folder)
  dataset = np.ndarray(shape=(len(image_files), image_size, image_size),
                         dtype=np.float32)
  print(folder)
  num_images = 0
  for image in image_files:
    image_file = os.path.join(folder, image)
    try:
      image_data = (ndimage.imread(image_file).astype(float) - 
                    pixel_depth / 2) / pixel_depth
      if image_data.shape != (image_size, image_size):
        raise Exception('Unexpected image shape: %s' % str(image_data.shape))
      dataset[num_images] = image_data
      num_images = num_images + 1
    except IOError as e:
      print('Could not read:', image_file, ':', e, '- it\'s ok, skipping.')
    
  dataset = dataset[0:num_images, :, :]
  if num_images < min_num_images:
    raise Exception('Much fewer images than expected: %d < %d' %
                    (num_images, min_num_images))
    
  print('Full dataset tensor:', dataset.shape)
  print('Mean:', np.mean(dataset))
  print('Standard deviation:', np.std(dataset))
  return dataset
        
def maybe_pickle(data_folders, min_num_images_per_class, force=False):
  dataset_names = []
  for folder in data_folders:
    set_filename = folder + '.pickle'
    dataset_names.append(set_filename)
    if os.path.exists(set_filename) and not force:
      print('%s already present - Skipping pickling.' % set_filename)
    else:
      print('Pickling %s.' % set_filename)
      dataset = load_letter(folder, min_num_images_per_class)
      try:
        with open(set_filename, 'wb') as f:
          pickle.dump(dataset, f, pickle.HIGHEST_PROTOCOL)
      except Exception as e:
        print('Unable to save data to', set_filename, ':', e)
  return dataset_names

train_datasets = maybe_pickle(train_folders, 45000)
test_datasets = maybe_pickle(test_folders, 1800)

print(straight_line)
print("train_datasets: \n_ _ _ _ _ _ _ _ _ _ _ _ _");
for it in train_datasets:
  print(it)
print("_ _ _ _ _ _ _ _ _ _ _ _ _")
print("Initialization of the matrix of weights...")


neural = Neural_Network()


def Load_data(directory):
  cur_list = []
  for it in directory:
    print("Extrating from", it)
    try:
      with open(it, 'rb') as op:
         single_set = pickle.load(op)
         cur_list.append(single_set)
    except Exception as Err:
       print(Err, ":Unable to load data correctly from", it)
  print("Extracting Complete.")
  print(straight_line)
  return cur_list

training_list = Load_data(train_datasets)
training_sets = []

for it in range(0, train_sets_number):
  current_set = []
  for jt in range(0, 10):
    for lt in range(0, 3):
      current_letter = (training_list[jt][it * 3 + lt], jt)
      current_set.append(current_letter)
  np.random.shuffle(current_set)
  training_sets.append(current_set)

def test():
  print(straight_line)
  print("Preparing tests...")
  tests = []
  testing_list = Load_data(test_datasets)
  for it in testing_list:
    np.random.shuffle(it)
  for it in range(0, 10):
    for jt in range(0, 10):
      cur_test = (testing_list[it][jt], it)
      tests.append(cur_test)
  print("Tests has been loaded.")

for iteration in range(0, iteration_value):
  if (iteration % 100 == 0):
    print("Training Set N.", iteration)
  neural.Training(training_sets[iteration])
  
print(straight_line)
print("Do you want to run Testing? (YES/NO)")
if (input() == "YES"): test()